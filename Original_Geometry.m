% Generating plots to display the elemental configurations for the
% situation at hand
figure
fprintf(' %5s \n','Generating plots for the original geometry of the system');
fprintf(' %5s \n',' ');
for index = 3:6
   xtemp0(1) = 0;
   xtemp0(2) = L0(1);
   xtemp0(index) = xtemp0(index-1) + (L0(index-1)*cosd(alpha0(index-1))) ;
   end
xtemp00= [max(xtemp0), max(xtemp0)+.025];
centerx = [.025+.05 .025+.05-.05*cosd(45)];
centery = [0 .05*sind(45)];
Ninteyx = [.025+.05 .025+.05];
Ninetyy = [0 .05];

for index = 3:6
   ytemp0(1:2) = 0;
   ytemp0(index) = ytemp0(index-1) + (L0(index-1)*sind(alpha0(index-1))) ;
end
ytemp00 = [0,0];
subplot(3,2,1)
label11 = cellstr(num2str([1:5]'));
label12 = cellstr(num2str([6]'));
plot(xtemp0,ytemp0,'-ok',xtemp00,ytemp00,'-ok',centerx,centery,Ninteyx,Ninetyy,'r')
text(xtemp0(1:end-1),ytemp0(1:end-1)+.008,label11);
text(xtemp00(end),ytemp0(end)+.008,label12);
axis([0 .2 -.06 .06])
title('4 Elements')


for index = 3:10
   xtemp1(1) = 0;
   xtemp1(2) = L1(1);
   xtemp1(index) = xtemp1(index-1) + (L1(index-1)*cosd(alpha1(index-1))) ;
end
xtemp11= [max(xtemp1), max(xtemp1)+.025];

for index = 3:10
   ytemp1(1:2) = 0;
   ytemp1(index) = ytemp1(index-1) + (L1(index-1)*sind(alpha1(index-1))) ;
end
ytemp11 = [0,0];
subplot(3,2,2)
label21 = cellstr(num2str([1:9]'));
label22 = cellstr(num2str([10]'));
plot(xtemp1,ytemp1,'-ok',xtemp11,ytemp11,'-ok',centerx,centery,Ninteyx,Ninetyy,'r')
text(xtemp1(1:end-1),ytemp1(1:end-1)+.008,label21);
text(xtemp11(end),ytemp11(end)+.008,label22);
axis([0 .2 -.06 .06])
title('8 Elements')


for index = 3:18
   xtemp2(1) = 0;
   xtemp2(2) = L2(1);
   xtemp2(index) = xtemp2(index-1) + (L2(index-1)*cosd(alpha2(index-1))) ;
end
xtemp22= [max(xtemp2), max(xtemp2)+.025];

for index = 3:18
   ytemp2(1:2) = 0;
   ytemp2(index) = ytemp2(index-1) + (L2(index-1)*sind(alpha2(index-1))) ;
end
ytemp22 = [0,0];
subplot(3,2,3)
label31 = cellstr(num2str([1:17]'));
label32 = cellstr(num2str([18]'));
plot(xtemp2,ytemp2,'-ok',xtemp22,ytemp22,'-ok',centerx,centery,Ninteyx,Ninetyy,'r')
text(xtemp2(1:end-1),ytemp2(1:end-1)+.008,label31);
text(xtemp22(end),ytemp22(end)+.008,label32);
axis([0 .2 -.06 .06])
title('16 Elements')

for index = 3:34
   xtemp3(1) = 0;
   xtemp3(2) = L3(1);
   xtemp3(index) = xtemp3(index-1) + (L3(index-1)*cosd(alpha3(index-1))) ;
end
xtemp33= [max(xtemp3), max(xtemp3)+.025];

for index = 3:34
   ytemp3(1:2) = 0;
   ytemp3(index) = ytemp3(index-1) + (L3(index-1)*sind(alpha3(index-1))) ;
end
ytemp33 = [0,0];
subplot(3,2,4)
label41 = cellstr(num2str([1:33]'));
label42 = cellstr(num2str([34]'));
plot(xtemp3,ytemp3,'-ok',xtemp33,ytemp33,'-ok',centerx,centery,Ninteyx,Ninetyy,'r')
text(xtemp3(1),ytemp3(1)+.008,label41(1));
text(xtemp3(2:10)-.006,ytemp3(2:10)+.008,label41(2:10));
text(xtemp3(11:18)+.004,ytemp3(11:18)+.008,label41(11:18));
text(xtemp3(19:26)+.004,ytemp3(19:26)-.008,label41(19:26));
text(xtemp3(27:33)-.006,ytemp3(27:33)-.008,label41(27:33));
text(xtemp33(end),ytemp33(end)+.008,label42);
axis([0 .2 -.06 .06])
title('32 Elements')

for index = 3:66
   xtemp4(1) = 0;
   xtemp4(2) = L4(1);
   xtemp4(index) = xtemp4(index-1) + (L4(index-1)*cosd(alpha4(index-1))) ;
end
xtemp44= [max(xtemp4), max(xtemp4)+.025];

for index = 3:66
   ytemp4(1:2) = 0;
   ytemp4(index) = ytemp4(index-1) + (L4(index-1)*sind(alpha4(index-1))) ;
end
ytemp44 = [0,0];
subplot(3,1,3)
label51 = cellstr(num2str([1:65]'));
label52 = cellstr(num2str([66]'));
plot(xtemp4,ytemp4,'-ok',xtemp44,ytemp44,'-ok',centerx,centery,Ninteyx,Ninetyy,'r')
text(xtemp4(1),ytemp4(1)+.008,label51(1));
text(xtemp4(2:17)-.002,ytemp4(2:17)+.01,label51(2:17));
text(xtemp4(18:33)+.002,ytemp4(18:33)+.01,label51(18:33));
text(xtemp4(34:49)+.002,ytemp4(34:49)-.01,label51(34:49));
text(xtemp4(50:65)-.002,ytemp4(50:65)-.01,label51(50:65));
text(xtemp44(end),ytemp44(end)+.008,label52);
axis([0 .2 -.06 .06])
title('64 Elements')

% for index = 3:130
%    xtemp5(1) = 0;
%    xtemp5(2) = L4(1);
%    xtemp5(index) = xtemp5(index-1) + (L5(index-1)*cosd(alpha5(index-1))) ;
% end
% xtemp55= [max(xtemp5), max(xtemp5)+.025];

% for index = 3:130
%    ytemp5(1:2) = 0;
%    ytemp5(index) = ytemp5(index-1) + (L5(index-1)*sind(alpha5(index-1))) ;
% end
% ytemp55 = [0,0];
% subplot(3,2,6)
% label61 = cellstr(num2str([1:129]'));
% label62 = cellstr(num2str([130]'));
% plot(xtemp5,ytemp5,'-ok',xtemp55,ytemp55,'-ok',centerx,centery)
% text(xtemp5(1),ytemp5(1)+.008,label61(1));
% text(xtemp5(2:33)-.01,ytemp5(2:33)+.01,label61(2:33));
% text(xtemp5(34:65)+.01,ytemp5(34:65)+.01,label61(34:65));
% text(xtemp5(66:97)+.01,ytemp5(66:97)-.01,label61(66:97));
% text(xtemp5(98:129)-.01,ytemp5(98:129)-.01,label61(98:129));
% text(xtemp55(end),ytemp55(end)+.008,label62);
% axis([0 .2 -.06 .06])
% title('64 Elements')