%% Input details into columns and rows
%% 4 Sides
clc; clear all;
fprintf(' %5s \n','Input desired force in N');
p1 = 'P = ';
AA = input(p1);
n0 = 4;
Element0 = (1:n0+2);
i0 = [1:n0+1 n0];
j0 = [2:5 2 6];
E0(1:6) = 72e9;
I0(1:6) = (.01^4)/12;
A0(1:6) = .01^2;
Fxi0(1:6) = zeros;
Fyi0(1:6) = zeros;
Mi0(1:6) = zeros;
Fxj0(1:5) = zeros;
Fxj0(6) = AA;
Fyj0(1:6) = zeros;
Mj0(1:6) = zeros;
L0(1) = .025;
L0(2:5) = .05/cosd(45);
L0(6) = .025;
h0(1:6) = .01;
alpha0(1) = 0;
alpha0(2) = 45;
alpha0(3) = -45;
alpha0(4) = 180+45;
alpha0(5) = 180-45;
alpha0(6) = 0;
for W = 1:6
ECT0(W) = struct('Element',Element0(W)','i',i0(W)','j',j0(W)','E',E0(W)',...
                 'I',I0(W)','A',A0(W)','Fxi',Fxi0(W)','Fyi',Fyi0(W)',...
                 'Mi',Mi0(W)','Fxj',Fxj0(W)','Fyj',Fyj0(W)','Mj',...
                  Mj0(W)','L',L0(W)','h',h0(W)','alpha',alpha0(W)');
end

% Print this table
fileID = fopen(['ectproj',num2str(n0),'.txt'],'w');
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f \r\n',n0+2,n0+2,0,0,0,0,0,0,0,0,0,0,0,0,0);
fprintf(fileID,'%5s %5s %5s %10s %10s %10s %10s %10s %10s %10s %10s %10s %9s %10s %13s \r\n', ' % Element','i','j','E (GPa)', 'I (m^4)', 'A (m^2)','Fxi (N)','Fyi (N)', 'Mi (Nm)','Fxj (N)','Fyj (N)','Mj (Nm)','L (m)','h (m)','alpha (deg)');
for Z = 1:6
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %5.2f %10.5f %10.5f %10.2f %13.5f %10.5f %10.5f %10.5f %10.5f \r\n',ECT0(Z).Element,ECT0(Z).i,ECT0(Z).j,ECT0(Z).E,ECT0(Z).I,ECT0(Z).A,ECT0(Z).Fxi,ECT0(Z).Fyi,ECT0(Z).Mi,ECT0(Z).Fxj,ECT0(Z).Fyj,ECT0(Z).Mj,ECT0(Z).L,ECT0(Z).h,ECT0(Z).alpha);
end
fclose(fileID);

fprintf(' %5s \n',['Generated element connectivity table for ',num2str(n0),' nodes']); 
%% 8 Sides 
n1 = 2;
a1 = 8; %4*n
IntAng1 = 180*(a1-2);
EachAng_2_1 = IntAng1/a1/2;
Element1 = (1:a1+2);
i1 = [1:a1+1 a1/2+2];
j1 = [2:a1+1 2 a1+2];
E1(1:a1+2) = 72e9;
I1(1:a1+2) = (.01^4)/12;
A1(1:a1+2) = .01^2;
Fyi1(1:a1+2) = zeros;
Fyj1(1:a1+2) = zeros;
L1(1) = .025;
L1(2:a1+1) = sqrt(2*.05^2*(1-cosd(45)));
h1(1:a1+2) = .01;
alpha1(1) = 0;
for blah1 = 0:6
    alpha1(blah1+2) = ((2*blah1+1)*EachAng_2_1)-(blah1*180);
end
alpha1(9) = 180-EachAng_2_1;
alpha1(10) = zeros;
Fxi1(1:10) = zeros;
Mi1(1:10) = zeros;
Fxj1(1:9) = zeros;
Fxj1(10) = AA;
Mj1(1:10) = zeros;
L1(a1+2) = .025;


for W = 1:10
ECT1(W) = struct('Element',Element1(W)','i',i1(W)','j',j1(W)','E',E1(W)','I',I1(W)','A',A1(W)','Fxi',Fxi1(W)','Fyi',Fyi1(W)','Mi',Mi1(W)','Fxj',Fxj1(W)','Fyj',Fyj1(W)','Mj',Mj1(W)','L',L1(W)','h',h1(W)','alpha',alpha1(W)');
end

%% Print this table
fileID = fopen(['ectproj',num2str(a1),'.txt'],'w');
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f \r\n',a1+2,a1+2,0,0,0,0,0,0,0,0,0,0,0,0,0);
fprintf(fileID,'%5s %5s %5s %10s %10s %10s %10s %10s %10s %10s %10s %10s %9s %10s %13s \r\n', ' % Element','i','j','E (GPa)', 'I (m^4)', 'A (m^2)','Fxi (N)','Fyi (N)', 'Mi (Nm)','Fxj (N)','Fyj (N)','Mj (Nm)','L (m)','h (m)','alpha (deg)');
for Z = 1:10
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %5.2f %10.5f %10.5f %10.2f %13.5f %10.5f %10.5f %10.5f %12.5f \r\n',ECT1(Z).Element,ECT1(Z).i,ECT1(Z).j,ECT1(Z).E,ECT1(Z).I,ECT1(Z).A,ECT1(Z).Fxi,ECT1(Z).Fyi,ECT1(Z).Mi,ECT1(Z).Fxj,ECT1(Z).Fyj,ECT1(Z).Mj,ECT1(Z).L,ECT1(Z).h,ECT1(Z).alpha);
end
fclose(fileID);

fprintf(' %5s \n',['Generated element connectivity table for ',num2str(a1),' nodes']);
%% 16 sides
a2 = 16; %4*n
IntAng2 = 180*(a2-2);
EachAng_2_2 = IntAng2/a2/2;
Element2 = (1:a2+2);
i2 = [1:a2+1 a2/2+2];
j2 = [2:a2+1 2 a2+2];
E2(1:a2+2) = 72e9;
I2(1:a2+2) = (.01^4)/12;
A2(1:a2+2) = .01^2;
Fyi2(1:a2+2) = zeros;
Fyj2(1:a2+2) = zeros;
L2(1) = .025;
L2(2:a2+1) = sqrt(2*.05^2*(1-cosd(180-2*EachAng_2_2)));
L2(a2+2) = .025;
h2(1:a2+2) = .01;
alpha2(1) = 0;
for foo = 0:14
alpha2(foo+2) = ((2*foo+1)*EachAng_2_2)-(foo*180); 
end
alpha2(17) = 180-EachAng_2_2;
alpha2(18) = zeros;
Fxi2(1:18) = zeros;
Mi2(1:18) = zeros;
Mj2(1:18) = zeros;
Fxj2(1:17) = zeros;
Fxj2(18) = AA;
for W = 1:a2+2
ECT2(W) = struct('Element',Element2(W)','i',i2(W)','j',j2(W)','E',E2(W)','I',I2(W)','A',A2(W)','Fxi',Fxi2(W)','Fyi',Fyi2(W)','Mi',Mi2(W)','Fxj',Fxj2(W)','Fyj',Fyj2(W)','Mj',Mj2(W)','L',L2(W)','h',h2(W)','alpha',alpha2(W)');
end

% Print this table
fileID = fopen(['ectproj',num2str(a2),'.txt'],'w');
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f \r\n',a2+2,a2+2,0,0,0,0,0,0,0,0,0,0,0,0,0);
fprintf(fileID,'%5s %5s %5s %10s %10s %10s %10s %10s %10s %10s %10s %10s %9s %10s %13s \r\n', ' % Element','i','j','E (GPa)', 'I (m^4)', 'A (m^2)','Fxi (N)','Fyi (N)', 'Mi (Nm)','Fxj (N)','Fyj (N)','Mj (Nm)','L (m)','h (m)','alpha (deg)');
for Z = 1:a2+2
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %5.2f %10.5f %10.5f %10.2f %13.5f %10.5f %10.5f %10.5f %10.5f \r\n',ECT2(Z).Element,ECT2(Z).i,ECT2(Z).j,ECT2(Z).E,ECT2(Z).I,ECT2(Z).A,ECT2(Z).Fxi,ECT2(Z).Fyi,ECT2(Z).Mi,ECT2(Z).Fxj,ECT2(Z).Fyj,ECT2(Z).Mj,ECT2(Z).L,ECT2(Z).h,ECT2(Z).alpha);
end
fclose(fileID);

fprintf(' %5s \n',['Generated element connectivity table for ',num2str(a2),' nodes']);
%% 32 Sides
a3 = 32; 
IntAng3 = 180*(a3-2);
EachAng_2_3 = IntAng3/a3/2;
Element3 = (1:a3+2);
i3 = [1:a3+1 a3/2+2];
j3 = [2:a3+1 2 a3+2];
E3(1:a3+2) = 72e9;
I3(1:a3+2) = (.01^4)/12;
A3(1:a3+2) = .01^2;
Fyi3(1:a3+2) = zeros;
Fyj3(1:a3+2) = zeros;
L3(1) = .025;
L3(2:a3+1) = sqrt(2*.05^2*(1-cosd(180-2*EachAng_2_3)));
L3(a3+2) = .025;
h3(1:a3+2) = .01;
alpha3(1) = 0;
for foo = 0:30
alpha3(foo+2) = ((2*foo+1)*EachAng_2_3)-(foo*180);
end
alpha3(33) = 180-EachAng_2_3;
alpha3(34) = zeros;
Fxi3(1:34) = zeros;
Mi3(1:34) = zeros;
Mj3(1:34) = zeros;
Fxj3(1:33) = zeros;
Fxj3(34) = AA;
for W = 1:a3+2
ECT3(W) = struct('Element',Element3(W)','i',i3(W)','j',j3(W)','E',E3(W)','I',I3(W)','A',A3(W)','Fxi',Fxi3(W)','Fyi',Fyi3(W)','Mi',Mi3(W)','Fxj',Fxj3(W)','Fyj',Fyj3(W)','Mj',Mj3(W)','L',L3(W)','h',h3(W)','alpha',alpha3(W)');
end

% Print this table
fileID = fopen(['ectproj',num2str(a3),'.txt'],'w');
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f \r\n',a3+2,a3+2,0,0,0,0,0,0,0,0,0,0,0,0,0);
fprintf(fileID,'%5s %5s %5s %10s %10s %10s %10s %10s %10s %10s %10s %10s %9s %10s %13s \r\n', ' % Element','i','j','E (GPa)', 'I (m^4)', 'A (m^2)','Fxi (N)','Fyi (N)', 'Mi (Nm)','Fxj (N)','Fyj (N)','Mj (Nm)','L (m)','h (m)','alpha (deg)');
for Z = 1:a3+2
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %5.2f %10.5f %10.5f %10.2f %13.5f %10.5f %10.5f %10.5f %10.5f \r\n',ECT3(Z).Element,ECT3(Z).i,ECT3(Z).j,ECT3(Z).E,ECT3(Z).I,ECT3(Z).A,ECT3(Z).Fxi,ECT3(Z).Fyi,ECT3(Z).Mi,ECT3(Z).Fxj,ECT3(Z).Fyj,ECT3(Z).Mj,ECT3(Z).L,ECT3(Z).h,ECT3(Z).alpha);
end
fclose(fileID);

fprintf(' %5s \n',['Generated element connectivity table for ',num2str(a3),' nodes']);
%% 64
a4 = 64; 
IntAng4 = 180*(a4-2);
EachAng_2_4 = IntAng4/a4/2;
Element4 = (1:a4+2);
i4 = [1:a4+1 a4/2+2];
j4 = [2:a4+1 2 a4+2];
E4(1:a4+2) = 72e9;
I4(1:a4+2) = (.01^4)/12;
A4(1:a4+2) = .01^2;
Fyi4(1:a4+2) = zeros;
Fyj4(1:a4+2) = zeros;
L4(1) = .025;
L4(2:a4+1) = sqrt(2*.05^2*(1-cosd(180-2*EachAng_2_4)));
L4(a4+2) = .025;
h4(1:a4+2) = .01;
alpha4(1) = 0;
for foo = 0:62
alpha4(foo+2) = ((2*foo+1)*EachAng_2_4)-(foo*180);
end
alpha4(65) = 180-EachAng_2_4;
alpha4(66) = zeros;
Fxi4(1:66) = zeros;
Mi4(1:66) = zeros;
Mj4(1:66) = zeros;
Fxj4(1:65) = zeros;
Fxj4(66) = AA;
for W = 1:a4+2
ECT4(W) = struct('Element',Element4(W)','i',i4(W)','j',j4(W)','E',E4(W)','I',I4(W)','A',A4(W)','Fxi',Fxi4(W)','Fyi',Fyi4(W)','Mi',Mi4(W)','Fxj',Fxj4(W)','Fyj',Fyj4(W)','Mj',Mj4(W)','L',L4(W)','h',h4(W)','alpha',alpha4(W)');
end

% Print this table
fileID = fopen(['ectproj',num2str(a4),'.txt'],'w');
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f \r\n',a4+2,a4+2,0,0,0,0,0,0,0,0,0,0,0,0,0);
fprintf(fileID,'%5s %5s %5s %10s %10s %10s %10s %10s %10s %10s %10s %10s %9s %10s %13s \r\n', ' % Element','i','j','E (GPa)', 'I (m^4)', 'A (m^2)','Fxi (N)','Fyi (N)', 'Mi (Nm)','Fxj (N)','Fyj (N)','Mj (Nm)','L (m)','h (m)','alpha (deg)');
for Z = 1:a4+2
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %5.2f %10.5f %10.5f %10.2f %13.5f %10.5f %10.5f %10.5f %10.5f \r\n',ECT4(Z).Element,ECT4(Z).i,ECT4(Z).j,ECT4(Z).E,ECT4(Z).I,ECT4(Z).A,ECT4(Z).Fxi,ECT4(Z).Fyi,ECT4(Z).Mi,ECT4(Z).Fxj,ECT4(Z).Fyj,ECT4(Z).Mj,ECT4(Z).L,ECT4(Z).h,ECT4(Z).alpha);
end
fclose(fileID);

save('L4','L4')
fprintf(' %5s \n',['Generated element connectivity table for ',num2str(a4),' nodes']);
fprintf(' %5s \n',' ');
%% 128 Sides
a5 = 128; 
IntAng5 = 180*(a5-2);
EachAng_2_5 = IntAng5/a5/2;
Element5 = (1:a5+2);
i5 = [1:a5+1 a5/2+2];
j5 = [2:a5+1 2 a5+2];
E5(1:a5+2) = 72e9;
I5(1:a5+2) = (.01^4)/12;
A5(1:a5+2) = .01^2;
Fyi5(1:a5+2) = zeros;
Fyj5(1:a5+2) = zeros;
L5(1) = .025;
L5(2:a5+1) = sqrt(2*.05^2*(1-cosd(180-2*EachAng_2_5)));
L5(a5+2) = .025;
h5(1:a5+2) = .01;
alpha5(1) = 0;
for foo = 0:126
alpha5(foo+2) = ((2*foo+1)*EachAng_2_5)-(foo*180);
end
alpha5(129) = 180-EachAng_2_5;
alpha5(130) = zeros;
Fxi5(1:130) = zeros;
Mi5(1:130) = zeros;
Mj5(1:130) = zeros;
Fxj5(1:129) = zeros;
Fxj5(130) = AA;
for W = 1:a5+2
ECT5(W) = struct('Element',Element5(W)','i',i5(W)','j',j5(W)','E',E5(W)','I',I5(W)','A',A5(W)','Fxi',Fxi5(W)','Fyi',Fyi5(W)','Mi',Mi5(W)','Fxj',Fxj5(W)','Fyj',Fyj5(W)','Mj',Mj5(W)','L',L5(W)','h',h5(W)','alpha',alpha5(W)');
end

% Print this table
fileID = fopen(['ectproj',num2str(a5),'.txt'],'w');
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f \r\n',a5+2,a5+2,0,0,0,0,0,0,0,0,0,0,0,0,0);
fprintf(fileID,'%5s %5s %5s %10s %10s %10s %10s %10s %10s %10s %10s %10s %9s %10s %13s \r\n', ' % Element','i','j','E (GPa)', 'I (m^4)', 'A (m^2)','Fxi (N)','Fyi (N)', 'Mi (Nm)','Fxj (N)','Fyj (N)','Mj (Nm)','L (m)','h (m)','alpha (deg)');
for Z = 1:a5+2
fprintf(fileID,'%7.2d %8.2d %6.2d %7.3d %13.11f %10.5f %5.2f %10.5f %10.5f %10.2f %13.5f %10.5f %10.5f %10.5f %10.5f \r\n',ECT5(Z).Element,ECT5(Z).i,ECT5(Z).j,ECT5(Z).E,ECT5(Z).I,ECT5(Z).A,ECT5(Z).Fxi,ECT5(Z).Fyi,ECT5(Z).Mi,ECT5(Z).Fxj,ECT5(Z).Fyj,ECT5(Z).Mj,ECT5(Z).L,ECT5(Z).h,ECT5(Z).alpha);
end
fclose(fileID);