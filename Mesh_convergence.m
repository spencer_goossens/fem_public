%% Finding the Displacemnt convergence
% Finding UN 
if Total_NO_EL == 6
   UN4 = U_i(3);
   
elseif Total_NO_EL == 10
   UN8 = U_i(4);
   
elseif Total_NO_EL == 18
   UN16 = U_i(6);
   
elseif Total_NO_EL == 34
   UN32 = U_i(10);
   
elseif Total_NO_EL == 66
   UN64 = U_i(18);

end

% Finding VN
if Total_NO_EL == 6
   VN4 = V_i(3);
   
elseif Total_NO_EL == 10
   VN8 = V_i(4);
   
elseif Total_NO_EL == 18
   VN16 = V_i(6);
   
elseif Total_NO_EL == 34
   VN32 = V_i(10);
   
elseif Total_NO_EL == 66
   VN64 = V_i(18);

end

% Finding ThetaN
if Total_NO_EL == 6
   ThetaN4 = Theta_i(3);
   
   
elseif Total_NO_EL == 10
   ThetaN8 = Theta_i(4);
   
elseif Total_NO_EL == 18
   ThetaN16 = Theta_i(6);
   
elseif Total_NO_EL == 34
   ThetaN32 = Theta_i(10);
   
elseif Total_NO_EL == 66
   ThetaN64 = Theta_i(18);

end

%%%%%%%%%%%%%%%%%%%%
% Finding QN
% Shape Functions for N=4 case
N1=1-3*(1/2)^2+2*(1/2)^3;
N2=(.0707/2)*(1-2*(1/2)+(1/2)^2);
N3=3*(1/2)^2-2*(1/2)^3;
N4=(.0707/2)*(-(1/2)+(1/2)^2);

if Total_NO_EL == 6
   UQ4 = N1*U_i(2)+N2*Theta_i(2)+N3*U_i(3)+N4*Theta_i(3);
   
elseif Total_NO_EL == 10
   UQ8 = U_i(3);
   
elseif Total_NO_EL == 18
   UQ16 = U_i(4);
   
elseif Total_NO_EL == 34
   UQ32 = U_i(6);
   
elseif Total_NO_EL == 66
   UQ64 = U_i(10);

end


% Finding VN
if Total_NO_EL == 6
   VQ4 = N1*V_i(2)+N2*Theta_i(2)+N3*V_i(3)+N4*Theta_i(3);
   
elseif Total_NO_EL == 10
   VQ8 = V_i(3);
   
elseif Total_NO_EL == 18
   VQ16 = V_i(4);
   
elseif Total_NO_EL == 34
   VQ32 = V_i(6);
   
elseif Total_NO_EL == 66
   VQ64 = V_i(10);


end

% Finding ThetaN
if Total_NO_EL == 6
   ThetaQ4 = 0;
   
elseif Total_NO_EL == 10
   ThetaQ8 = Theta_i(3);
   
elseif Total_NO_EL == 18
   ThetaQ16 = Theta_i(4);
   
elseif Total_NO_EL == 34
   ThetaQ32 = Theta_i(6);
   
elseif Total_NO_EL == 66
   ThetaQ64 = Theta_i(10);

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Finding strain convergence 
% This is done using the formula epsilon = [B][d] - y[C][d] where [B] and
% [C] are the first and second derivatives of the shape functions
N1_fun = @(x,L)1-3*(x/L)^2+2*(x/L)^3;
N2_fun = @(x,L)x*(1-2*(x/L)+(x/L)^2);
N3_fun = @(x,L)3*(x/L)^2-2*(x/L)^3;
N4_fun = @(x,L)x*(-(x/L)+(x/L)^2);

B1_fun = @(x,L)(-6*x)/(L^2)+6*x^2/L^3;
B2_fun = @(x,L)1-4*(x/L)+3*x^2/L^2;
B3_fun = @(x,L)6*x/(L^2)-6*x^2/(L^3);
B4_fun = @(x,L)-2*(x/L)+3*(x/L)^2;

C1_fun = @(x,L)-6/(L^2)+12*x/L^3;
C2_fun = @(x,L)-4/L+6*x/L^2;
C3_fun = @(x,L)6/(L^2)-12*x/(L^3);
C4_fun = @(x,L)-2/L+6*x/(L^2);
R = .05;
No = .01/2;
Ni = -.01/2;

%%%%%%%%%%%%%%% Epsilon at No and Ni %%%%%%%%%%%%%%%%%%


if Total_NO_EL == 6
    Lfun = L(2);
T(1,1) = c(2);
T(1,2) = -s(2);
T(2,1) = s(2);
T(2,2) = c(2);
T(3,3) = 1;

T(4,4) = c(2);
T(4,5) = -s(2);
T(5,4) = s(2);
T(5,5) = c(2);
T(6,6) = 1;

   UN4b = U_i(3);
   ThetaN4b = Theta_i(3);
   VN4b = V_i(3);
   UN4a = U_i(2);
   ThetaN4a = Theta_i(2);
   VN4a = V_i(2);
   C_mat4 = [C1_fun(Lfun,Lfun) C2_fun(Lfun,Lfun) C3_fun(Lfun,Lfun) C4_fun(Lfun,Lfun)];
   dtemp = [UN4a VN4a ThetaN4a UN4b VN4b ThetaN4b]';
   d4temp = T*dtemp;
   d4 = [d4temp(2) d4temp(3) d4temp(5) d4temp(6)]';
   Eps_x4No = -No*C_mat4*d4;
   Eps_x4Ni = -Ni*C_mat4*d4;

   
elseif Total_NO_EL == 10
    index = 3;
    Lfun = L(index);
T8(1,1) = c(index);
T8(1,2) = -s(index);
T8(2,1) = s(index);
T8(2,2) = c(index);
T8(3,3) = 1;

T8(4,4) = c(index);
T8(4,5) = -s(index);
T8(5,4) = s(index);
T8(5,5) = c(index);
T8(6,6) = 1;

   UN8b = U_i(4);
   ThetaN8b = Theta_i(4);
   VN8b = V_i(4);
   UN8a = U_i(3);
   ThetaN8a = Theta_i(3);
   VN8a = V_i(3);
   B_mat8 = [B1_fun(1,1) B2_fun(1,1) B3_fun(1,1) B4_fun(1,1)];
   C_mat8 = [C1_fun(Lfun,Lfun) C2_fun(Lfun,Lfun) C3_fun(Lfun,Lfun) C4_fun(Lfun,Lfun)];
   dtemp8 = [UN8a VN8a ThetaN8a UN8b VN8b ThetaN8b]';
   d8temp = T8*dtemp8;
   d8 = [d8temp(2) d8temp(3) d8temp(5) d8temp(6)]';
   Eps_x8No = -No*C_mat8*d8;
   Eps_x8Ni = -Ni*C_mat8*d8;

   
elseif Total_NO_EL == 18
      index2 = 5;
      Lfun = L(index2);
T16(1,1) = c(index);
T16(1,2) = -s(index);
T16(2,1) = s(index);
T16(2,2) = c(index);
T16(3,3) = 1;

T16(4,4) = c(index);
T16(4,5) = -s(index);
T16(5,4) = s(index);
T16(5,5) = c(index);
T16(6,6) = 1;

   UN16b = U_i(index2+1);
   ThetaN16b = Theta_i(index2+1);
   VN16b = V_i(index2+1);
   UN16a = U_i(index2);
   ThetaN16a = Theta_i(index2);
   VN16a = V_i(index2);
   B_mat16 = [B1_fun(1,1) B2_fun(1,1) B3_fun(1,1) B4_fun(1,1)];
   C_mat16 = [C1_fun(Lfun,Lfun) C2_fun(Lfun,Lfun) C3_fun(Lfun,Lfun) C4_fun(Lfun,Lfun)];
   dtemp16 = [UN16a VN16a ThetaN16a UN16b VN16b ThetaN16b]';
   d16temp = T16*dtemp16;
   d16 = [d16temp(2) d16temp(3) d16temp(5) d16temp(6)]';
   Eps_x16No = -No*C_mat16*d16;
   Eps_x16Ni = -Ni*C_mat16*d16;

   
 elseif Total_NO_EL == 34
      index3 = 9;
      Lfun = L(index3);
T32(1,1) = c(index3);
T32(1,2) = -s(index3);
T32(2,1) = s(index3);
T32(2,2) = c(index3);
T32(3,3) = 1;

T32(4,4) = c(index3);
T32(4,5) = -s(index3);
T32(5,4) = s(index3);
T32(5,5) = c(index3);
T32(6,6) = 1;

   UN32b = U_i(index3+1);
   ThetaN32b = Theta_i(index3+1);
   VN32b = V_i(index3+1);
   UN32a = U_i(index3);
   ThetaN32a = Theta_i(index3);
   VN32a = V_i(index3);
   B_mat32 = [B1_fun(1,1) B2_fun(1,1) B3_fun(1,1) B4_fun(1,1)];
   C_mat32 = [C1_fun(Lfun,Lfun) C2_fun(Lfun,Lfun) C3_fun(Lfun,Lfun) C4_fun(Lfun,Lfun)];
   dtemp32 = [UN32a VN32a ThetaN32a UN32b VN32b ThetaN32b]';
   d32temp = T32*dtemp32;
   d32 = [d32temp(2) d32temp(3) d32temp(5) d32temp(6)]';
   Eps_x32No = -No*C_mat32*d32;
   Eps_x32Ni = -Ni*C_mat32*d32;

   
elseif Total_NO_EL == 66
         index4 = 17;
         Lfun = L(index4);
T64(1,1) = c(index4);
T64(1,2) = -s(index4);
T64(2,1) = s(index4);
T64(2,2) = c(index4);
T64(3,3) = 1;

T64(4,4) = c(index4);
T64(4,5) = -s(index4);
T64(5,4) = s(index4);
T64(5,5) = c(index4);
T64(6,6) = 1;

   UN64b = U_i(index4+1);
   ThetaN64b = Theta_i(index4+1);
   VN64b = V_i(index4+1);
   UN64a = U_i(index4);
   ThetaN64a = Theta_i(index4);
   VN64a = V_i(index4);
   B_mat64 = [B1_fun(1,1) B2_fun(1,1) B3_fun(1,1) B4_fun(1,1)];
   C_mat64 = [C1_fun(Lfun,Lfun) C2_fun(Lfun,Lfun) C3_fun(Lfun,Lfun) C4_fun(Lfun,Lfun)];
   dtemp64 = [UN64a VN64a ThetaN64a UN64b VN64b ThetaN64b]';
   d64temp = T64*dtemp64;
   d64 = [d64temp(2) d64temp(3) d64temp(5) d64temp(6)]';
   Eps_x64No = -No*C_mat64*d64;
   Eps_x64Ni = -Ni*C_mat64*d64;
end

save(['UN',num2str(Total_NO_EL-2)],['UN',num2str(Total_NO_EL-2)])
save(['VN',num2str(Total_NO_EL-2)],['VN',num2str(Total_NO_EL-2)])
save(['ThetaN',num2str(Total_NO_EL-2)],['ThetaN',num2str(Total_NO_EL-2)])
save(['UQ',num2str(Total_NO_EL-2)],['UQ',num2str(Total_NO_EL-2)])
save(['VQ',num2str(Total_NO_EL-2)],['VQ',num2str(Total_NO_EL-2)])
save(['ThetaQ',num2str(Total_NO_EL-2)],['ThetaQ',num2str(Total_NO_EL-2)])
% Strain Convergance
save(['EpsNi',num2str(Total_NO_EL-2)],['Eps_x',num2str(Total_NO_EL-2),...
    'Ni'])
save(['EpsNo',num2str(Total_NO_EL-2)],['Eps_x',num2str(Total_NO_EL-2),...
    'No'])