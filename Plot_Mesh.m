figure
for index = [1 2 4 8 16]
   load(['UN',num2str(index*4)])
   load(['VN',num2str(index*4)])
   load(['ThetaN',num2str(index*4)])
   load(['UQ',num2str(index*4)])
   load(['VQ',num2str(index*4)])
   load(['ThetaQ',num2str(index*4)])
   load(['EpsNi',num2str(index*4)])
   load(['EpsNo',num2str(index*4)])
end

XMESH = [4 8 16 32 64];
YMESHUN = [UN4 UN8 UN16 UN32 UN64];
YMESHUQ = [UQ4 UQ8 UQ16 UQ32 UQ64];
YMESHVN = [VN4 VN8 VN16 VN32 VN64];
YMESHVQ = [VQ4 VQ8 VQ16 VQ32 VQ64];
YMESHThetaN = [ThetaN4 ThetaN8 ThetaN16 ThetaN32 ThetaN64];
YMESHThetaQ = [ThetaQ4 ThetaQ8 ThetaQ16 ThetaQ32 ThetaQ64];

diffUN = diff(YMESHUN);
diffVN = diff(YMESHVN);
diffThetaN = diff(YMESHThetaN);
diffUQ = diff(YMESHUQ);
diffVQ = diff(YMESHVQ);
diffThetaQ = diff(YMESHThetaQ);


subplot(2,2,1)
plot(XMESH,YMESHUN,'k',XMESH,YMESHUQ,'r')
title('U Displacement at N and Q')
xlabel('Number of nodes')
ylabel('Displacement (m)')
legend('UN','UQ','Location','Northwest')
subplot(2,2,2)
plot(XMESH,YMESHVN,'k',XMESH,YMESHVQ,'r')
xlabel('Number of nodes')
ylabel('Displacement (m)')
title('V Displacement at N and Q')
legend('VN','VQ','Location','Northwest')
subplot(2,2,3)
plot(XMESH,YMESHThetaQ,'r',XMESH,YMESHThetaN,'k')
xlabel('Number of nodes')
ylabel('Displacement (rad)')
title('Theta Displacement at N and Q')
legend('ThetaN','ThetaQ','Location','Northwest')


XEPS = [4 8 16 32 64];
YEPSNi = [Eps_x4Ni Eps_x8Ni Eps_x16Ni Eps_x32Ni Eps_x64Ni];
YEPSNo = [Eps_x4No Eps_x8No Eps_x16No Eps_x32No Eps_x64No];
subplot(2,2,4)
plot(XEPS,YEPSNi,'k',XEPS,YEPSNo,'r')
xlabel('Number of nodes')
ylabel('Strain ')
title('Epsilon_{xx} Convergance at Points Ni and No')
legend('Epsilon_{xx}_{Ni}','Epsilon_{xx}_{No}','Location','Northwest')



