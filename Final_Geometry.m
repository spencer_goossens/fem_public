clc; clear all;
Project
figure
for index = 3:6
   xtempL0(1) = 0+(U_i(1)*10^4);
   xtempL0(2) = xtempL0(1) + L0(1)+(U_i(2)*10^4);
   xtempL0(index) = xtempL0(index-1)+((L0(index-1)*cosd(alpha0(index-1)))+(U_i(index)*10^4)) ;
end
xtemp00= [max(xtempL0), max(xtempL0)+.025];

for index = 3:6
   ytempL0(1) = 0+(V_i(1)*10^4);
   ytempL0(2) = ytempL0(1)+(V_i(2)*10^4);
   ytempL0(index) = ytempL0(index-1) + ((L0(index-1)*sind(alpha0(index-1)))+(V_i(index)*10^4)) ;
end
ytemp00 = [0,0];
subplot(3,2,1)
plot(xtempL0,ytempL0,xtemp00,ytemp00)


% for index = 3:10
%    xtemp1(1) = 0;
%    xtemp1(2) = L1(1);
%    xtemp1(index) = xtemp1(index-1) + (L1(index-1)*cosd(alpha1(index-1))) ;
% end
% xtemp11= [max(xtemp1), max(xtemp1)+.025];
% 
% for index = 3:10
%    ytemp1(1:2) = 0;
%    ytemp1(index) = ytemp1(index-1) + (L1(index-1)*sind(alpha1(index-1))) ;
% end
% ytemp11 = [0,0];
% subplot(3,2,2)
% plot(xtemp1,ytemp1,xtemp11,ytemp11)
% 
% 
% 
% for index = 3:18
%    xtemp2(1) = 0;
%    xtemp2(2) = L2(1);
%    xtemp2(index) = xtemp2(index-1) + (L2(index-1)*cosd(alpha2(index-1))) ;
% end
% xtemp22= [max(xtemp2), max(xtemp2)+.025];
% 
% for index = 3:18
%    ytemp2(1:2) = 0;
%    ytemp2(index) = ytemp2(index-1) + (L2(index-1)*sind(alpha2(index-1))) ;
% end
% ytemp22 = [0,0];
% subplot(3,2,3)
% plot(xtemp2,ytemp2,xtemp22,ytemp22)
% 
% 
% for index = 3:34
%    xtemp3(1) = 0;
%    xtemp3(2) = L0(1);
%    xtemp3(index) = xtemp3(index-1) + (L3(index-1)*cosd(alpha3(index-1))) ;
% end
% xtemp33= [max(xtemp3), max(xtemp3)+.025];
% 
% for index = 3:34
%    ytemp3(1:2) = 0;
%    ytemp3(index) = ytemp3(index-1) + (L3(index-1)*sind(alpha3(index-1))) ;
% end
% ytemp33 = [0,0];
% subplot(3,2,4)
% plot(xtemp3,ytemp3,xtemp33,ytemp33)
% 
% 
% for index = 3:66
%    xtemp4(1) = 0;
%    xtemp4(2) = L0(1);
%    xtemp4(index) = xtemp4(index-1) + (L4(index-1)*cosd(alpha4(index-1))) ;
% end
% xtemp44= [max(xtemp4), max(xtemp4)+.025];
% 
% for index = 3:66
%    ytemp4(1:2) = 0;
%    ytemp4(index) = ytemp4(index-1) + (L4(index-1)*sind(alpha4(index-1))) ;
% end
% ytemp44 = [0,0];
% subplot(3,2,5)
% plot(xtemp4,ytemp4,xtemp44,ytemp44)