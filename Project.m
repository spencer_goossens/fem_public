%%%%% This script implements the FEA Method.  In order to run, simply
%%%%% run the script and input the force and number of elements you would
%%%%% like to mesh with.  If running for the first time it is recommended
%%%%% to go in order from 4 8 16 32 64 elements.
%% Pre procesing 
% Generating the ECT tables
Generate_ect 
fprintf('%5s \n','Input the desired number of elements to mesh with');
Nodes_inp = 'Desired number of nodes = ';
Desired_nodes = input(Nodes_inp);
% Loading ECT
ECT_matrix_full = load(['ectproj',num2str(Desired_nodes),'.txt']); 
% Creating ECT (not including NO_NODES and Total_NO_EL)
ECT_matrix = ECT_matrix_full(2:end,:); 
% Reading total number of elements
Total_NO_EL = ECT_matrix_full(1,1); 
% Reading total number of nodes
Total_NO_NODES = ECT_matrix_full(1,2); 
fprintf(' %5s \n',['System running for ',num2str(Total_NO_EL-2), ...
       ' elements']);
fprintf(' %5s \n',' ');
%% Pre-allocation
% Pre-allocate memory for certain arrays to speed up calculations
Elements = zeros;i = zeros;j = zeros;E = zeros;I = zeros;A = zeros;...
alpha = zeros;Fxi = zeros;Fyi = zeros;Mi = zeros;Fxj = zeros;...
Fyj = zeros;Mj = zeros;L = zeros;h = zeros;k = zeros;a = zeros;...
BC_Node = zeros;BC_DOF = zeros;BC_Value = zeros;U_i = zeros;V_i = zeros;...
Theta_i = zeros;Eps_norm = zeros;Sigma_norm = zeros;Sigma_Bend = zeros;...
ArcLength = zeros;

%% Loading of Element Connectivity
% Assigns different arrays to their respective values (i.e Element numbers,
% E, I etc...
for no_el = 1:Total_NO_EL
    Elements(no_el,1) = ECT_matrix(no_el,1);
    i(no_el,1) = ECT_matrix(no_el,2);
    j(no_el,1) = ECT_matrix(no_el,3);
    E(no_el,1) = ECT_matrix(no_el,4);
    I(no_el,1) = ECT_matrix(no_el,5);
    A(no_el,1) = ECT_matrix(no_el,6);
    Fxi(no_el,1) = ECT_matrix(no_el,7);
    Fyi(no_el,1) = ECT_matrix(no_el,8);
    Mi(no_el,1) = ECT_matrix(no_el,9);
    Fxj(no_el,1) = ECT_matrix(no_el,10);
    Fyj(no_el,1) = ECT_matrix(no_el,11);
    Mj(no_el,1) = ECT_matrix(no_el,12);
    L(no_el,1) = ECT_matrix(no_el,13);
    h(no_el,1) = ECT_matrix(no_el,14);    
    alpha(no_el,1) = ECT_matrix(no_el,15);
end

Element_Connectivity_Table = table(Elements,i,j,E,I,A,Fxi,Fyi,Mi,Fxj,...
                                      Fyj,Mj,L,h,alpha);

%% Assembaly of Global Stiffness Matrix
%%Global Matrix
K_Global_proj_2

%% Assembaly of Global Force Matrix
Global_Force

%% Boundary Conditions
% Implementing the Boundary Conditions using the penalty method.  Reading
% the Boundary Conditions
fprintf(' %5s \n','Assessing  boundary conditions of the system using', ...
'the penalty method');
fprintf(' %5s \n',' ');
Boundary_Conditions
% Loading the Boundary Conditions and assigning them to arrays
Input_BCs = load('BC.txt');
Total_BCs = Input_BCs(1,1);
fprintf(' %5s \n',['There are ',num2str(Total_BCs),' for this system']);
fprintf(' %5s \n',' ' );
for index = 2:Total_BCs+1
    BC_Node(index-1) = Input_BCs(index,1);
    BC_DOF(index-1) = Input_BCs(index,2);
    BC_Value(index-1) = Input_BCs(index,3);  
end


fprintf(' %5s \n',['u(',num2str(BC_Node(1)),') = ',num2str(BC_Value(1))]);
fprintf(' %5s \n',['v(',num2str(BC_Node(2)),') = ',num2str(BC_Value(2))]);
fprintf(' %5s \n',['Theta(',num2str(BC_Node(3)),') = '...
    ,num2str(BC_Value(3))]);
fprintf(' %5s \n',' ');

% Creating a penalty number based on the largest entry in K_G 
if Total_NO_NODES < 128
penalty = 10000*max(max(K_G));
fprintf(' %5s \n',['For a ',num2str(Total_NO_NODES-2),' noded ring']);
fprintf(' %5s \n',['Penalty number = ',num2str(penalty),...
    ' which is 10,000 times the maximum entry in K_G']);
else 
penalty = 100*max(max(K_G));    
fprintf(' %5s \n',['For a ',num2str(Total_NO_NODES-2),' noded ring']);
fprintf(' %5s \n',['Penalty number = ',num2str(penalty),...
    ' which is 10,000 times the maximum entry in K_G']);
fprintf(' %5s \n',' ');
end
% Creating a zero matrix where dim(pen_matrix) = 3*NO_NODES x 3*NO_NODES
pen_matrix = zeros(last); 
% Assigning the penalty number to the diagonals where the BC's -> 0
for index = 1:Total_BCs
pen_matrix(BC_DOF(index),BC_DOF(index)) = penalty; 
end
% Ammending the Global Stiffness Matrix to reflect the Boundary Conditions
K_G = K_G + pen_matrix; 
fprintf(' %5s \n','Assessed the boundary conditions by implementation',...
    ' of the penalty method');
fprintf(' %5s \n',' ');
%% Solution
% Solving the system for the displacements and rotations
D = K_G\Force_Global;
fprintf(' %5s \n','Solved for the nodal displacements');
fprintf(' %5s \n',' ');
%% Post-Processing
% Recovering the displacements of each element and  then creating a
% structured array to easily read these diplacements
for index = 0:Total_NO_EL-1
   U_i(index+1) = D(1+3*index); 
   V_i(index+1) = D(2+3*index);
   Theta_i(index+1) = D(3+3*index);
   d(index+1) = struct('U_i',U_i(index+1),'V_i',V_i(index+1),'Theta_i',...
                            Theta_i(index+1));
end
% Calculating the stress and then the strain in each element
for index = 1:Total_NO_EL-2
Tloc(1,1) = c(index);
Tloc(1,2) = -s(index);
Tloc(2,1) = s(index);
Tloc(2,2) = c(index);
Tloc(3,3) = 1;

Tloc(4,4) = c(index);
Tloc(4,5) = -s(index);
Tloc(5,4) = s(index);
Tloc(5,5) = c(index);
Tloc(6,6) = 1;
    
U1 = U_i(index);
U2 = U_i(index+1);
V1 = V_i(index);
V2 = V_i(index+1);
Theta1 = Theta_i(index);
Theta2 = Theta_i(index+1);
d_temp1 = [U1 V1 Theta1 U2 V2 Theta2]';
d_temp2 = Tloc*d_temp1;
U1_prime = d_temp2(1);
U2_prime = d_temp2(4);
   Eps_norm(index) = (U2_prime-U1_prime)/L(index); 
   Sigma_norm(index) = E(index)*Eps_norm(index);
end

Tloc2(1,1) = c(Total_NO_EL-1);
Tloc2(1,2) = -s(Total_NO_EL-1);
Tloc2(2,1) = s(Total_NO_EL-1);
Tloc2(2,2) = c(Total_NO_EL-1);
Tloc2(3,3) = 1;

Tloc2(4,4) = c(Total_NO_EL-1);
Tloc2(4,5) = -s(Total_NO_EL-1);
Tloc2(5,4) = s(Total_NO_EL-1);
Tloc2(5,5) = c(Total_NO_EL-1);
Tloc2(6,6) = 1;
    
U12 = U_i(Total_NO_EL-1);
U22 = U_i(Total_NO_EL);
V12 = V_i(Total_NO_EL-1);
V22 = V_i(Total_NO_EL);
Theta12 = Theta_i(Total_NO_EL-1);
Theta22 = Theta_i(Total_NO_EL);
d_temp12 = [U12 V12 Theta12 U22 V22 Theta22]';
d_temp22 = Tloc2*d_temp12;
U1_prime2 = d_temp22(1);
U2_prime2 = d_temp22(4);
Eps_norm(Total_NO_EL-1) = (U2_prime2-U1_prime2)/L(end-1); 
Sigma_norm(Total_NO_EL-1) = E(Total_NO_EL-1)*Eps_norm(Total_NO_EL-1);

Tloc3(1,1) = c(Total_NO_EL);
Tloc3(1,2) = -s(Total_NO_EL);
Tloc3(2,1) = s(Total_NO_EL);
Tloc3(2,2) = c(Total_NO_EL);
Tloc3(3,3) = 1;

Tloc3(4,4) = c(Total_NO_EL);
Tloc3(4,5) = -s(Total_NO_EL);
Tloc3(5,4) = s(Total_NO_EL);
Tloc3(5,5) = c(Total_NO_EL);
Tloc3(6,6) = 1;
    
U13 = U_i(Total_NO_EL);
U23 = U_i(Total_NO_EL/2+1);
V13 = V_i(Total_NO_EL);
V23 = V_i(Total_NO_EL/2+1);
Theta13 = Theta_i(Total_NO_EL);
Theta23 = Theta_i(Total_NO_EL/2+1);
d_temp13 = [U13 V13 Theta13 U23 V23 Theta23]';
d_temp23 = Tloc3*d_temp13;
U1_prime3 = d_temp23(1);
U2_prime3 = d_temp23(4);
Eps_norm(Total_NO_EL) = (U2_prime3-U1_prime3)/L(end); 
Sigma_norm(Total_NO_EL) = E(Total_NO_EL)*Eps_norm(Total_NO_EL);
   
fprintf(' %5s \n','Calculated the elemental stresses and strains');
fprintf(' %5s \n',' ');
display(K_G,['Global Stiffness Matrix ',num2str(Total_NO_EL-2),...
    ' elements'])
display(Force_Global,['Global Force Matrix ',num2str(Total_NO_EL-2),...
    ' elements'])

%% Mesh Convergance
Mesh_convergence
fprintf(' %5s \n','Executed a mesh convergance');
fprintf(' %5s \n',' ');
% Plotting the convergance of the meshes
fprintf(' %5s \n','Generating plots for the mesh convergance of the',...
    ' system');
fprintf(' %5s \n',' ');

%% Variation Converged 
% Saving the axial stess for the converged number of nodes.
if Total_NO_EL == 66


% Plotting the variation of axial stress along the top of the ring for N =
% 64
fprintf(' %5s \n','Generating plots for the variation of stress along',...
    ' the top half of the ring');
fprintf(' %5s \n',' ');
Nodes = 3:34;

for index = 2:33
T(1,1) = c(index);
T(1,2) = -s(index);
T(2,1) = s(index);
T(2,2) = c(index);
T(3,3) = 1;

T(4,4) = c(index);
T(4,5) = -s(index);
T(5,4) = s(index);
T(5,5) = c(index);
T(6,6) = 1;

UA = U_i(index);
VA = V_i(index);
ThetaA = Theta_i(index);
UB = U_i(index+1);
VB = V_i(index+1);
ThetaB = Theta_i(index+1);
C_Mat64 = [C1_fun(Lfun,Lfun) C2_fun(Lfun,Lfun) C3_fun(Lfun,Lfun) C4_fun(Lfun,Lfun)];
d_glob = [UA VA ThetaA UB VB ThetaB]';

d_prime = T*d_glob;
d_bend = [d_prime(2) d_prime(3) d_prime(5) d_prime(6)]';
   
Sigma_Bend(index-1) = -E(index).*0*C_Mat64*d_bend;
Sigma_Bend_Ni(index-1) = -E(index).*(R+Ni)*C_Mat64*d_bend;
Sigma_Bend_No(index-1) = -E(index).*(R+No)*C_Mat64*d_bend;
Sigma_Bend_Si(index-1) = -E(index).*(R+Ni)*C_Mat64*d_bend;
Sigma_Bend_So(index-1) = -E(index).*(R+No)*C_Mat64*d_bend;
   
end

Sigma_norm_top = Sigma_norm(2:33);
for index = 1:32
Stress_axial_converged(index) = Sigma_norm_top(index)+Sigma_Bend(index);
Stress_axial_converged_Ni(index) = Sigma_norm_top(index)+...
    Sigma_Bend_Ni(index);
Stress_axial_converged_No(index) = Sigma_norm_top(index)+...
    Sigma_Bend_No(index);
end
Sigma_norm_bot = Sigma_norm(34:65);
for index = 1:32
Stress_axial_converged_Si(index) = Sigma_norm_bot(index)+...
    Sigma_Bend_Si(index);
Stress_axial_converged_So(index) = Sigma_norm_bot(index)+...
    Sigma_Bend_So(index);
end

figure
subplot(2,1,1)
plot(Nodes,Stress_axial_converged)
title(['The Axial Stress of the Top Half of the Ring for P = ',num2str(AA),' N with 64 Elements'])
xlabel('Node Number')
ylabel('Axial Stress Nm^{-2}')


for index = 3:33
ArceLength(1) = L4(2);
ArcLength(index-1) = ArcLength(index-2) + L4(index);
end

subplot(2,1,2)
plot(ArcLength,Sigma_Bend)
title(['The Bending Stress of the Top Half of the Ring for P = ',num2str(AA),' N with 64 Elements'])
xlabel('Arc Length (m)')
ylabel('Bending Stress Nm^{-2}')
end
fprintf(' %5s \n','Plotting the original geometry of the system');
fprintf(' %5s \n',' ');
fprintf(' %5s \n','Plotting the mesh convergance');
fprintf(' %5s \n',' ');
fprintf(' %5s \n','Plotting the variation of stress along the top',...
    ' half of the ring');
fprintf(' %5s \n',' ');
%% Calibration factor
% To preform a virtual calibration follow these steps:
% 1) Run the program as is after un-commenting this section with a load of
% 100 N and 64 Nodes.
% 2) Change the permission of fileID from 'w' to 'a' and comment out the
% first fprintf line.
% 3) Run the system again as many times as desired varying the load.

% EpsNi = Stress_axial_converged_Ni(16)/E(17);
%        
% EpsNo = Stress_axial_converged_No(16)/E(17);
%        
% EpsSi = Stress_axial_converged_Si(16)/E(17);
%       
% EpsSo = Stress_axial_converged_So(16)/E(17);
%       
% EpsAvg = (abs(EpsNi)+abs(EpsNo)+abs(EpsSi)+abs(EpsSo))/4;
%       
% fileID = fopen('Calibration.txt','w');
% fprintf(fileID,'%7s %8s %6s %7s %13s %10s \r\n','% Load (N)',...
%         %'No','Ni','So','Si','Average');
% fprintf(fileID,'%5.3d %10.3d %10.3d %10.3d %10.3d %10.3d \r\n',AA,...
%            EpsNi,EpsNo,EpsSi,EpsSo,EpsAvg);
% fclose(fileID);

fprintf(' %5s \n','Calibrating the system');
fprintf(' %5s \n',' ');
Cal_mat = load('Calibration.txt');
Load = Cal_mat(:,1);
Eps_No = Cal_mat(:,2);
Eps_Ni = Cal_mat(:,3);
Eps_So = Cal_mat(:,4);
Eps_Si = Cal_mat(:,5);
Eps_Avg = Cal_mat(:,6);

Cal_Table = table(Load,Eps_No,Eps_Ni,Eps_So,Eps_Si,Eps_Avg);
save('Calibration_table','Cal_Table');

for index = 1:3
cal_factor(index) = Load(index)/Eps_Avg(index);
end

Cal_factor_avg = mean(cal_factor);
if Total_NO_EL == 66
Plot_Mesh

fprintf(' %5s \n',['The optimal number of elements is ',...
    num2str(Total_NO_EL-2)]);
fprintf(' %5s \n',' ');

else
fprintf(' %5s \n','The optimal number of elements has not been found,',...
    ' please increase mesh');
fprintf(' %5s \n',' ');   
end
% Plotting the Elemental configurations
Original_Geometry 