% Calculating the Global Stiffness Matrix
% Defining conditions under which the Global Stiffness Matrix will be
% calculated
MaxNodes1 = max(i)+1; 
MaxNodes2 = max(j);
kill=0;
% Calculating the local stiffness matrix as well as the local
% transformation matrix.  Once these are calculated, the tranformation is
% carried out and then the resulting matrix (K_temp) is added to K_G.
while MaxNodes1 == MaxNodes2 && kill==0

K_G(1:MaxNodes1*3,1:MaxNodes1*3) = zeros;

c = cosd(alpha);
s = sind(alpha);

for index = 1:Total_NO_EL
k(index) = E(index)*A(index)/L(index);
a(index) = E(index)*I(index)/L(index); 

end

for index = 1:Total_NO_EL
K_local(1,1) = k(index);
K_local(1,4) = -1*k(index);

K_local(2,2) = 12*a(index)/L(index)^2;
K_local(2,3) = 6*a(index)/L(index);
K_local(2,5) = -12*a(index)/L(index)^2;
K_local(2,6) = 6*a(index)/L(index);

K_local(3,2) = 6*a(index)/L(index);
K_local(3,3) = 4*a(index);
K_local(3,5) = -6*a(index)/L(index);
K_local(3,6) = 2*a(index);

K_local(4,1) = -1*k(index);
K_local(4,4) = k(index);

K_local(5,2) = -12*a(index)/L(index)^2;
K_local(5,3) = -6*a(index)/L(index);
K_local(5,5) = 12*a(index)/L(index)^2;
K_local(5,6) = -6*a(index)/L(index);

K_local(6,2) = 6*a(index)/L(index);
K_local(6,3) = 2*a(index);
K_local(6,5) = -6*a(index)/L(index);
K_local(6,6) = 4*a(index);

fprintf(' %5s \n',['Generated local element stiffness matrix for i = ',num2str(i(index)),' and j = ',num2str(j(index))]);

T(1,1) = c(index);
T(1,2) = -s(index);
T(2,1) = s(index);
T(2,2) = c(index);
T(3,3) = 1;

T(4,4) = c(index);
T(4,5) = -s(index);
T(5,4) = s(index);
T(5,5) = c(index);
T(6,6) = 1;

K_temp = transpose(T)*K_local*T;

if i(index)+1 == j(index)

K_G(3*j(index),3*j(index)-5:3*j(index)) = K_G(3*j(index),3*j(index)-5:3*j(index))+K_temp(6,:);
K_G(3*j(index)-1,3*j(index)-5:3*j(index)) = K_G(3*j(index)-1,3*j(index)-5:3*j(index))+K_temp(5,:);
K_G(3*j(index)-2,3*j(index)-5:3*j(index)) = K_G(3*j(index)-2,3*j(index)-5:3*j(index))+K_temp(4,:);
K_G(3*j(index)-3,3*j(index)-5:3*j(index)) = K_G(3*j(index)-3,3*j(index)-5:3*j(index))+K_temp(3,:);
K_G(3*j(index)-4,3*j(index)-5:3*j(index)) = K_G(3*j(index)-4,3*j(index)-5:3*j(index))+K_temp(2,:);
K_G(3*j(index)-5,3*j(index)-5:3*j(index)) = K_G(3*j(index)-5,3*j(index)-5:3*j(index))+K_temp(1,:);

else % elseif i(index)~= j(index) && i(index) > j(index)
K_G(3*j(index),3*j(index)-2:3*j(index)) = K_G(3*j(index),3*j(index)-2:3*j(index))+K_temp(6,4:6);
K_G(3*j(index),3*i(index)-2:3*i(index)) = K_G(3*j(index),3*i(index)-2:3*i(index))+K_temp(6,1:3);
K_G(3*j(index)-1,3*j(index)-2:3*j(index)) = K_G(3*j(index)-1,3*j(index)-2:3*j(index))+K_temp(5,4:6);
K_G(3*j(index)-1,3*i(index)-2:3*i(index)) = K_G(3*j(index)-1,3*i(index)-2:3*i(index))+K_temp(5,1:3);
K_G(3*j(index)-2,3*j(index)-2:3*j(index)) = K_G(3*j(index)-2,3*j(index)-2:3*j(index))+K_temp(4,4:6);
K_G(3*j(index)-2,3*i(index)-2:3*i(index)) = K_G(3*j(index)-2,3*i(index)-2:3*i(index))+K_temp(4,1:3);

K_G(3*i(index),3*j(index)-2:3*j(index)) = K_G(3*i(index),3*j(index)-2:3*j(index))+K_temp(3,4:6);
K_G(3*i(index),3*i(index)-2:3*i(index)) = K_G(3*i(index),3*i(index)-2:3*i(index))+K_temp(3,1:3);
K_G(3*i(index)-1,3*j(index)-2:3*j(index)) = K_G(3*i(index)-1,3*j(index)-2:3*j(index))+K_temp(2,4:6);
K_G(3*i(index)-1,3*i(index)-2:3*i(index)) = K_G(3*i(index)-1,3*i(index)-2:3*i(index))+K_temp(2,1:3);
K_G(3*i(index)-2,3*j(index)-2:3*j(index)) = K_G(3*i(index)-2,3*j(index)-2:3*j(index))+K_temp(1,4:6);
K_G(3*i(index)-2,3*i(index)-2:3*i(index)) = K_G(3*i(index)-2,3*i(index)-2:3*i(index))+K_temp(1,1:3);


end
% kill is set to 1 to stop the process 
kill=1;

end
end
% Testing to see if the Global Stiffness Matrix is indeed symmetric. 1
% represents symmetry, 0 is asymmetric
K_G_Symmetric = issymmetric(K_G);
fprintf(' %5s \n',' ');
fprintf(' %5s \n','Generated global element stiffness matrix');
fprintf(' %5s \n',' ');