% Writing Global Force Vector by reading the individual forces and moments
% and then assigning them to their respective positions in the Global Force
% Vector
Force_Global = zeros;
last = Total_NO_NODES*3;
for index = 0:Total_NO_NODES-1
    Force_Global(1+3*index) = Fxj(index+1); % Fx
    Force_Global(2+3*index) = Fyj(index+1); % Fy
    Force_Global(3+3*index) = Mj(index+1);  % M
    
    fprintf(' %5s \n',['Generated global force vector for element ',num2str(index+1)]);
end
% Making the Force Vector into a column vector
Force_Global = transpose(Force_Global);
fprintf(' %5s \n',' ');
fprintf(' %5s \n','Generated global force vector');
fprintf(' %5s \n',' ');